const express = require('express');
const userRouter = express.Router();

const mysql = require('mysql2');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '123qwe',
    database: 'sqldb',
    insecureAuth: true
});

db.connect((err) => {
    if (err) throw err
    console.log('User Router connected!')
})

//cors requirement
userRouter.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next()
});

//get all users
userRouter.get('/', (req, res) => {
    let sql = 'SELECT * FROM user';
    db.query(sql, (err, results) => {
        if (err) throw err
        return res.json({
            data: results
        })
    })
})

//get an user by id
userRouter.get('/:id', (req, res) => {
    let sql = 'SELECT * FROM user where user_id = ?';
    db.query(sql, [req.params.id], (err, result) => {
        if (err) throw err
        return res.json({
            data: user
        })
    })
})

//create new user
//userRouter.post('/')

module.exports = userRouter;