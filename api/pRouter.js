const express = require('express');
const pRouter = express.Router();
const mysql = require('mysql2');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '123qwe',
    database: 'sqldb',
    insecureAuth: true
});

db.connect((err) => {
    if (err) throw err;
    console.log('Products Router connected!');
})

// cors requirement
pRouter.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });

pRouter.use(function(req, res, next) {
    res.setHeader("Content-Type", "application/x-www-form-urlencoded");
    next();
});

// favico.ico err handling
pRouter.get('/favicon.ico', (req, res, next) => {
    res.status(204);
})

//get all products in stock
pRouter.get('/', (req, res, next) => {
    let sql = 'SELECT * FROM product WHERE stock > 0';
    db.query(sql, (err, results) => {
        if (err) throw err
        return res.json({
            results: results
        })
    })
})

// get product by id
pRouter.get('/:id', (req, res, next) => {
    let sql = `SELECT * FROM product WHERE id=?`;
    db.query(sql,[req.params.id], (err, result) => {
        if (err) throw err
        return res.json({ 
            result: result
        })
    })
})

// create new product
pRouter.post('/', (req, res, next) => {
    // product form info
    const name = req.body.product.name;
    const price = req.body.product.price;
    let description = req.body.product.description;
    let stock = req.body.product.stock;
    let categoryId = req.body.product.categoryId;

    if( !name || !price ) {
        res.status(400).json({ err: `Required field missing` })
    } else if (!description || !stock || !categoryId) {
        description = 'no description';
        stock = '0'
        categoryId = '0'
    }

     let sql = `INSERT INTO product (name, price, description, stock, category_id) VALUES (?, ?, ?, ?, ?)`;

    db.query(sql, [
        name,
        price,
        description,
        stock,
        categoryId
    ], (err, result) => {
        if (err) throw err
        // id of the last inserted product 
        db.query(`SELECT * FROM product WHERE id=?`, [result.insertId], (err, product) => {
            if (err) throw err
            return res.status(200).json({
                result: result
            })
        })
    })
})

// update product
pRouter.put('/:id',  (req, res, next) =>  {

    const name = req.body.product.name;
    const price = req.body.product.price;
    let description = req.body.product.description;
    let stock = req.body.product.stock;
    let categoryId = req.body.product.categoryId;

    const sql = `UPDATE product SET name=?, price=?, description=?, stock=?, category_id=? WHERE id=?`;
    db.query(sql, [
        name,
        price,
        description,
        stock,
        categoryId,
        req.params.id
    ], (err) => {
        if (err) throw err;
        db.query(`SELECT * FROM product WHERE id=?`,[req.params.id], (err, result) => {
            if(err) throw err
            return res.json({ result: result })
        })
    })
})

// delete product
pRouter.delete('/:id', (req, res) => {
    let sql = `DELETE FROM product WHERE id=?`;
    db.query(sql, [req.params.id], (err) => {
        if(err) throw err
        res.sendStatus(200)
    })
})

module.exports = pRouter;