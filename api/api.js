const express = require('express');
const apiRouter = express.Router();

// routers
const pRouter = require('./pRouter');
const userRouter = require('./userRouter');
const categoryRouter = require('./categoryRouter');

apiRouter.use('/user', userRouter);
apiRouter.use('/product', pRouter);
apiRouter.use('/category', categoryRouter);

module.exports = apiRouter;