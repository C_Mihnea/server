const express = require('express');
const categoryRouter = express.Router();

const mysql = require('mysql2');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '123qwe',
    database: 'sqldb',
    insecureAuth: true
})

db.connect((err) => {
    if (err) throw err
    console.log( 'Category Router connected!' )
})

// get all categories
categoryRouter.get('/', (req, res, next) => {
    let sql = 'SELECT * FROM category'
    db.query(sql, (err, results) => {
        if (err) throw err
        return res.json({
            data: results
        })
    })
})

// get category by id
categoryRouter.get('/:id', (req, res, next) => {
    let sql = `SELECT * FROM category WHERE id = ${req.params.id}`
    db.query(sql, (err, result) => {
        if (err) throw err
        return res.json({ 
            data: result
        })
    })
})

// create new category
categoryRouter.post('/', (req, res, next) => {

    const name = req.body.name;
    const itemOrder = req.body.itemOrder === undefined ? 0 : req.body.itemOrder;
    const parent = req.body.parent === undefined ? 'none' : req.body.parent;

    if (!name) {
        return res.status(400).json({ err: 'Name is required!' });
    } 

    let sql = `INSERT INTO category (name, item_order, parent) VALUES (?, ?, ?)`
    db.query(sql,[
        name, itemOrder, parent
    ],(err, result) => {
        if (err) throw err
        db.query(`SELECT * FROM category WHERE id=${result.insertId}`, (err, result) => {
            if (err) throw err
            return res.json({
                data: result
            })
        })
    })
})

// update a category
categoryRouter.put('/:id', (req, res, next) => {
    
    const name = req.body.name;
    const itemOrder = req.body.itemOrder;
    const parent = req.body.parent;
    const id = req.params.id

    let sql =`UPDATE category SET name = ?, item_order = ?, parent = ? WHERE id=?`;
   
    if (!name || !itemOrder || !parent ) {
        res.status(400).json({
            err: 'All fields are required'
        })
    }
    
    db.execute(sql, [
        name, 
        itemOrder, 
        parent,
        id
    ], (err, result) => {
        if (err) throw err
        db.query(`SELECT * FROM category WHERE id=?`,[id] ,(err, result) => {
            if (err) throw err
            return res.json({
                data: result
            })
        })
    })
})

// delete a category
categoryRouter.delete('/:id', (req, res, next) => {
    let sql = `DELETE FROM category WHERE id=?`
    db.query(sql, [req.params.id], (err, result) => {
        if (err) throw err
        res.sendStatus(200);
    })
})

module.exports = categoryRouter;