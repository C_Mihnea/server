const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const apiRouter = require('./api/api');

const app = express();
const PORT = process.env.PORT || 4000;

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.use('/', apiRouter);

app.listen(PORT, () => {
    console.log(`MySQL listening on port ${PORT}...`)
})

module.exports = app;